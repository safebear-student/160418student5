package com.safebear.tasklist.model;

import org.assertj.core.api.Assertions;
import org.junit.Test;

import java.time.LocalDate;

public class TaskTest {

    LocalDate localDate = LocalDate.now();
    protected Task task = new Task(1L, "Mop the floors", localDate, false);

    @Test
    public void creation(){

        Assertions.assertThat(task.getId()).isEqualTo(1L);

    }

}
