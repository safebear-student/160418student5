package com.safebear.tasklist.apitests;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.parsing.Parser;
import org.junit.Before;
import org.junit.Test;

import static com.jayway.restassured.RestAssured.get;
import static org.hamcrest.Matchers.equalTo;

public class ApiTestsIT extends BaseApi{

    @Test
    public void testTasksEndPoint(){

        get("/" + CONTEXT + "/api/tasks")
                .then()
                .assertThat()
                .statusCode(200);

    }

}
